from fasm import compile_fasm

import argparse

parser = argparse.ArgumentParser(
    prog='FASM compiler',
    description='Compile fasm code into VM byte-code file',
)

parser.add_argument(
    'input',
    type=argparse.FileType('r'),
    help='fasm input file path',
)

parser.add_argument(
    'output',
    type=argparse.FileType('wb'),
    help='output file path with byte-code',
)

args = parser.parse_args()
with args.input as fasm_file, args.output as res_file:
    compile_fasm(fasm_file, res_file)
