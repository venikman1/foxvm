from op_codes import OP_CODES

from abc import ABC, abstractmethod
import re

LABEL_RE = re.compile(r"LABEL\s+([^\W\d]\w*)")
CHAR_RE = re.compile(r"'(\w|!|\\n|\\\\)'")


class CommandI(ABC):
    @abstractmethod
    def write_byte_code(self, byte_code_file):
        pass


class EmptyCommand(CommandI):
    def write_byte_code(self, byte_code_file):
        pass


class LabelCommand(EmptyCommand):
    def __init__(self, label_name):
        self.label_name = label_name


class OpCommand(CommandI):
    def __init__(self, op_code, args):
        self.op_code = op_code
        self.args = args

    def write_byte_code(self, byte_code_file):
        if len(self.args) == 1 and self.args[0][0] == 'value':
            args_byte = self.args[0][1]
        elif len(self.args) == 1 and self.args[0][0] == 'register':
            args_byte = self.args[0][1]
        elif len(self.args) == 2 and self.args[0][0] == self.args[1][0] == 'register':
            args_byte = self.args[0][1] | (self.args[1][1] << 4)
        else:
            raise RuntimeError(f'Strange args {self.args}')
        byte_code_file.write(bytes([self.op_code, args_byte]))
        print([self.op_code, args_byte])


def parse_arg(arg: str):
    if arg.startswith('R'):
        rnum = int(arg[1:])
        if not 0 <= rnum < 16:
            raise RuntimeError(f'Unknown register {arg}')
        return 'register', rnum
    if char_match := CHAR_RE.match(arg):
        char = char_match.group(1)
        if char == '\\\\':
            value = ord('\\')
        elif char == '\\n':
            value = ord('\n')
        else:
            value = ord(char)
        if not 0 <= value < 256:
            raise RuntimeError(f'Value {char} is converted to {value} and is not in byte range')
        return 'value', value
    return 'value', int(arg)


def parse_line(line: str):
    comment = line.find(';')
    if comment >= 0:
        line = line[:comment].strip().capitalize()
    if not line:
        return EmptyCommand()
    if label_match := LABEL_RE.match(line):
        return LabelCommand(label_match.group(1))

    tokens = line.split(' ')
    if len(tokens) > 3:
        raise RuntimeError('Command can be maximum 3 token long')
    op = tokens[0]
    if op not in OP_CODES:
        raise RuntimeError(f'Unknown op {op}')
    args = tokens[1:]
    return OpCommand(OP_CODES[op], [parse_arg(arg) for arg in args])


def compile_fasm(fasm_file, byte_code_file):
    for line in fasm_file:
        command = parse_line(line)
        command.write_byte_code(byte_code_file)
