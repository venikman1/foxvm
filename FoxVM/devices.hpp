#pragma once

#include "fxbus.hpp"

#include <iostream>

namespace foxlib {
    class TestTerminal : public BusConnectionI {
    public:
        TestTerminal(std::uint8_t address) : address(address) {}

        bool on_send_package(std::uint8_t address, std::uint8_t data) override {
            if (address == this->address) {
                std::cout.put(static_cast<char>(data));
                return true;
            }
            return false;
        }

        std::optional<std::uint8_t> on_recieve_package(std::uint8_t address) override {
            if (address == this->address) {
                return static_cast<std::uint8_t>(std::cin.get());
            }
            return std::nullopt;
        }

    private:
        std::uint8_t address;
    };

}
