#pragma once

#include <cstddef>
#include <vector>
#include <concepts>
#include <cstdint>


namespace foxlib {
	/*
		Simple object that describes and contains information about VM RAM memory with minimum addressable cell is 1 byte.
	*/
	class Memory {
	public:
		Memory(std::size_t memory_size): memory(memory_size) {
			
		}

		std::uint8_t& operator[](std::size_t address) {
			return memory[address];
		}

		std::uint8_t operator[](std::size_t address) const {
			return memory[address];
		}

		std::size_t size() const {
			return memory.size();
		}

	private:
		std::vector<std::uint8_t> memory;
	};

	class StartupROM : private Memory {
	public:
		StartupROM(const Memory& memory) : Memory(memory) {}
		StartupROM(Memory&& memory) : Memory(memory) {}

		std::uint8_t operator[](std::size_t address) const {
			return Memory::operator[](address);
		}

		using Memory::size;

	private:
	};

	template <class Memory>
	concept RWMemory = requires(Memory memo, std::size_t address) {
		{memo[address]} -> std::same_as<std::uint8_t&>;
		{memo.size()} -> std::same_as<std::size_t>;
	};

	template <class Memory>
	concept ROMemory = requires(const Memory memo, std::size_t address) {
		{memo[address]} -> std::same_as<std::uint8_t>;
		{memo.size()} -> std::same_as<std::size_t>;
	} && !requires(Memory memo, std::size_t address) {
		{memo[address]} -> std::same_as<std::uint8_t&>;
	};

}
