#pragma once

#include "instruction_set.hpp"
#include "memory.hpp"
#include "fxbus.hpp"
#include "devices.hpp"


namespace foxlib {
	class VM {
	public:
		VM(std::size_t mem_size) : term(1), memory(mem_size), cunit(&memory) {
			bus.connect(&term);
			bus.connect(cunit.get_connection_pointer());
		}

		// Generate function that takes a StartupROM instance and loads it into memory starting at 0x0000
		template<ROMemory MemoryType>
		void load_startup_rom(const MemoryType& rom) {
            // Load the startup ROM into memory
            for (int i = 0; i < rom.size(); i++) {
                memory[i] = rom[i];
            }
        }

		void run() {
            while (!cunit.step()) {
				bus.resolve_queue(10);
            }
        }

	private:
		FXBus bus;
		TestTerminal term;
		Memory memory;
		ControlUnit<Memory> cunit;

	};
}
