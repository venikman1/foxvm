#pragma once

#include "memory.hpp"
#include "fxbus.hpp"

#include <cstdint>
#include <stdexcept>
#include <array>


namespace foxlib {
	/* Every command accepts two operands like this if not stated otherwise
	*    OP_CODE A0 A1
	*    A0 is destination registed
	*    A1 is source registed
	*
	*  Registers are 64 bit
	*  All arithmetic operations is done inside ring Z_(2^64)
	*/
	enum OpCode : std::uint8_t {
		/* Casual commands */
		NOP = 0,   // Do nothing
		MOV = 1,   // Copy A1 to A0
		SWP = 2,   // Swap A1 and A0
		LC0 = 3,   // Load constant 1st lowest part into R0 register (LCL VAL), where VAL is 8-bit number
		LC1 = 4,   // Load constant 2nd lowest part into R0 register (LCL VAL), where VAL is 8-bit number
		LC2 = 5,   // Load constant 3rd lowest part into R0 register (LCL VAL), where VAL is 8-bit number
		LC3 = 6,   // Load constant 4th lowest part into R0 register (LCL VAL), where VAL is 8-bit number

		/* Arithmetic operations */
		ADD = 10,  // Add A1 to A0
		SUB = 11,  // Subtract A1 from A0
		MLT = 12,  // Multiply A1 to A0
		DIV = 13,  // Integer division A0 on A1 and store into A0
		MOD = 14,  // Calculate modulus of division A0 on A1 and store into A0
		NEG = 15,  // Write negative number of A1 to A0
		INC = 16,  // A0 = A1 + 1
		DEC = 17,  // A0 = A1 - 1

		
		/* Conditioning (adresses must be aligned to 2) */
		JMP = 20,  // Jump to A0 address, A0 register value must be aligned to 2
		JIZ = 21,  // Jump to A0 address, if A1 register is zero
		JINZ = 22, // Jump to A0 address, if A1 register is not zero
		JIP = 23,  // Jump to A0 address, if A1 register highest bit is 0
		JINP = 24, // Jump to A0 address, if A1 register highest bit is 1

		/* Logic operations */
		AND = 30,  // Do bitwise and of A1 and A0 and store to A0
		OR = 31,   // Do bitwise or of A1 and A0 and store to A0
		XOR = 32,  // Do bitwise xor of A1 and A0 and store to A0
		LSH = 33,  // Do left shift of binary representation A0 for A1 bits, add 0 at rights
		RSH = 34,  // Do right shift of binary representation A0 for A1 bits, add 0 at left
		RSHS = 35, // Do right shift of binary representation A0 for A1 bits, add highest bit at left
		NOT = 36,  // Do bitwise not of A1 and store to A0
		
		/* Memory operations */
		LDB = 40,  // Load 8-bit memory from address A0 to A1
		LDW = 41,  // Load 16-bit memory from address A0 to A1
		LDDW = 42, // Load 32-bit memory from address A0 to A1
		LDQW = 43, // Load 64-bit memory from address A0 to A1
		STRB = 44, // Store 8-bit data from A1 to memory at address of A0
		STRW = 45, // Store 16-bit data from A1 to memory at address of A0
		STRDW = 46,// Store 32-bit data from A1 to memory at address of A0
		STRQW = 47,// Store 64-bit data from A1 to memory at address of A0
		

		/* VM control operations */
		GCI = 50,  // Store current instruction address to the A0
		HLT = 51,  // Halt execution until next interruption

		/* Communications */
		/*
			FXBUS is bus protocol for communication between VMs and other devices.
			Devices can work in two modes: master and slave.
			Master mode allows to iniciate reading and writing operations. Slave mode allows to respond to master.
			VM can work in both modes at the same time.

			Every 8-bit packet comes with an address on which every device can respond.
		*/
		FXSND = 60, // Send 8-bit data from A1 with the address of reciever channel A0
		FXRCV = 61, // Ask for recieving 8-bit with the address of sender channel A0
		// Create op for getting last recieved data
		FXGET = 62,  // Read recieved data into A0
		/* 
			FXRCV operation is not blocking. If there is no immediate data to recieve, A1 will be set to 0. In other way A1 will be set to recieved data with 1 at highest bit.
		*/
		FXST = 63  // Get status of FXBUS into A0
		/*
			Status bits mask (lowest)
			0b00000001 - ACK recieved on last send (FXSND op)
				It means that slave recieved and aknowledged data
			0b00000010 - Data pending on send (FXSND op)
				It means that sending operation is in progress
			If data is not pending and not aknowledged, it means that slave is not responding

			0b00000100 - Data recieved on recieve (FXRCV op)
				It means that slave send his data
			0b00001000 - Data pending on recieve (FXRCV op)
				It means that recieving operation is in progress
			If data is not pending and not recieved, it means that slave is not responding

			0b00010000 - Master mode
                It means that FXBUS is in master mode
		*/
		

	};
	
	template <RWMemory MemoryType>
	class ControlUnit {
	private:
		struct InstructionInfo {
			OpCode op_code;
			union {
				std::uint8_t value;
				struct {
					unsigned int a0 : 4;
					unsigned int a1 : 4;
				} registers;
			} operands;
		};
	public:
		ControlUnit(MemoryType* memory = nullptr) : memory(memory) {

		}

		bool step() {
			if (!memory) {
                throw std::runtime_error("Memory is not set");
            }
			if (current_instruction + 1 > memory->size()) {
				return true;
			}
			InstructionInfo instruction = read_instruction(current_instruction);
			return do_instruction(instruction);
		}

		BusConnectionI* get_connection_pointer() {
			return &master_bus_connection;
		}
		
	private:
		template <unsigned int part>
		std::uint8_t get_part(std::uint64_t value) {
			static_assert(part < 8, "Part must be less than 8");
            return (value >> (part * 8u)) & 0xFFu;
        }

		template <unsigned int part>
		std::uint64_t set_part(std::uint64_t value, std::uint8_t part_value) {
			static_assert(part < 8, "Part must be less than 8");
            return (value & ~(static_cast<std::uint64_t>(0xFFu) << (part * 8u))) | (static_cast<std::uint64_t>(part_value) << (part * 8u));
        }

		
		template <unsigned int... parts>
		std::uint64_t set_parts(std::uint64_t value, auto ... part_values) {
			return ((value = set_part<parts>(value, part_values)), ...);
		}

		template <unsigned int... parts>
		void get_parts_into_memory(std::uint64_t value, std::size_t address) {
			(((*memory)[address + parts] = get_part<parts>(value)), ...);
		}

		template <unsigned int... parts>
		std::uint64_t set_parts_from_memory(std::uint64_t value, std::size_t address) {
			return set_parts<parts...>(value, (*memory)[address + parts]...);
		}

		std::uint64_t bit_state() {
			return 0ull;
		}

		std::uint64_t bit_state(bool first, auto ... values) {
			return (bit_state(values...) << 1) | (first ? 1ull : 0ull);
		}

		bool do_instruction(const InstructionInfo& instruction) {
			bool jump_intruction = false;
			switch (instruction.op_code) {
			case NOP:
				break;
			case MOV:
				registers[instruction.operands.registers.a0] = registers[instruction.operands.registers.a1];
				break;
			case SWP:
				std::swap(registers[instruction.operands.registers.a0], registers[instruction.operands.registers.a1]);
				break;
			case LC0:
				registers[0] = set_part<0>(registers[0], instruction.operands.value);
				break;
			case LC1:
				registers[0] = set_part<1>(registers[0], instruction.operands.value);
				break;
			case LC2:
				registers[0] = set_part<2>(registers[0], instruction.operands.value);
				break;
			case LC3:
				registers[0] = set_part<3>(registers[0], instruction.operands.value);
				break;
			case ADD:
				registers[instruction.operands.registers.a0] += registers[instruction.operands.registers.a1];
				break;
			case SUB:
				registers[instruction.operands.registers.a0] -= registers[instruction.operands.registers.a1];
				break;
			case MLT:
				registers[instruction.operands.registers.a0] *= registers[instruction.operands.registers.a1];
				break;
			case DIV:
				registers[instruction.operands.registers.a0] /= registers[instruction.operands.registers.a1];
				break;
			case MOD:
				registers[instruction.operands.registers.a0] %= registers[instruction.operands.registers.a1];
				break;
			case NEG:
				registers[instruction.operands.registers.a0] = static_cast<std::uint64_t>(-static_cast<std::int64_t>(registers[instruction.operands.registers.a1]));
				break;
			case INC:
				registers[instruction.operands.registers.a0] = registers[instruction.operands.registers.a1] + 1;
				break;
			case DEC:
				registers[instruction.operands.registers.a0] = registers[instruction.operands.registers.a1] - 1;
				break;
			case JMP:
				current_instruction = registers[instruction.operands.registers.a0];
				jump_intruction = true;
				break;
			case JIZ:
				if (!registers[instruction.operands.registers.a1]) {
					current_instruction = registers[instruction.operands.registers.a0];
					jump_intruction = true;
				}
				break;
			case JINZ:
				if (registers[instruction.operands.registers.a1]) {
					current_instruction = registers[instruction.operands.registers.a0];
					jump_intruction = true;
				}
				break;
			case JIP:
				if (static_cast<std::int64_t>(registers[instruction.operands.registers.a1]) >= 0) {
					current_instruction = registers[instruction.operands.registers.a0];
					jump_intruction = true;
				}
				break;
			case JINP:
				if (static_cast<std::int64_t>(registers[instruction.operands.registers.a1]) < 0) {
					current_instruction = registers[instruction.operands.registers.a0];
					jump_intruction = true;
				}
				break;
			case AND:
				registers[instruction.operands.registers.a0] &= registers[instruction.operands.registers.a1];
				break;
			case OR:
				registers[instruction.operands.registers.a0] |= registers[instruction.operands.registers.a1];
				break;
			case XOR:
				registers[instruction.operands.registers.a0] ^= registers[instruction.operands.registers.a1];
				break;
			case LSH:
				registers[instruction.operands.registers.a0] <<= registers[instruction.operands.registers.a1];
				break;
			case RSH:
				registers[instruction.operands.registers.a0] >>= registers[instruction.operands.registers.a1];
				break;
			case RSHS:
				registers[instruction.operands.registers.a0] = static_cast<std::int64_t>(registers[instruction.operands.registers.a0]) >> registers[instruction.operands.registers.a1];
				break;
			case NOT:
				registers[instruction.operands.registers.a0] = ~registers[instruction.operands.registers.a1];
				break;
			case LDQW:
				registers[instruction.operands.registers.a1] = set_parts_from_memory<4, 5, 6, 7>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
			case LDDW:
				registers[instruction.operands.registers.a1] = set_parts_from_memory<2, 3>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
			case LDW:
				registers[instruction.operands.registers.a1] = set_parts_from_memory<1>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
			case LDB:
				registers[instruction.operands.registers.a1] = set_parts_from_memory<0>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
				break;
			case STRQW:
				get_parts_into_memory<4, 5, 6, 7>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
			case STRDW:
				get_parts_into_memory<2, 3>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
			case STRW:
				get_parts_into_memory<1>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
			case STRB:
				get_parts_into_memory<0>(registers[instruction.operands.registers.a1], registers[instruction.operands.registers.a0]);
				break;
			case GCI:
				registers[instruction.operands.registers.a0] = current_instruction;
				break;
			case HLT:
				return true;
				break;
			case FXSND:
				master_bus_connection.send_data(registers[instruction.operands.registers.a0], registers[instruction.operands.registers.a1]);
				break;
			case FXRCV:
				master_bus_connection.recieve_data(registers[instruction.operands.registers.a0]);
				break;
			case FXGET:
                registers[instruction.operands.registers.a0] = master_bus_connection.get_recieved_data();
                break;
			case FXST:
                registers[instruction.operands.registers.a0] = bit_state(
					master_bus_connection.is_send_complete(),
					master_bus_connection.is_send_pending(),
					master_bus_connection.is_recieve_complete(), 
					master_bus_connection.is_recieve_pending(),
					1
				);
                break;
			default:
				throw std::runtime_error("Unknown instruction");
			}
			if (!jump_intruction) {
				current_instruction += 2;
			}
			return false;
		}

		InstructionInfo read_instruction(std::size_t address) {
			if (address & 1u) {
				throw std::runtime_error("Address is not aligned for instruction read");
			}
			InstructionInfo result;
			result.op_code = static_cast<OpCode>((*memory)[address]);
			std::uint8_t args = (*memory)[address + 1];

			if (result.op_code >= OpCode::LC0 && result.op_code <= OpCode::LC3) {
				result.operands.value = args;
			}
			else {
				result.operands.registers.a0 = args & 0xF;
				result.operands.registers.a1 = args >> 4u;
			}
			return result;
		}

		MasterBusConnection master_bus_connection;
		MemoryType* memory;
		std::size_t current_instruction = 0;
		std::array<std::uint64_t, 16> registers;

	};
}