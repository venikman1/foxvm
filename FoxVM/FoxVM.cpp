#include "headers/vm.hpp"

#include <iostream>
#include <fstream>


int main()
{
    foxlib::VM vm(1024);


    foxlib::Memory prog(1024);
    std::ifstream file("C:\\git_reps\\fox_vm\\fasm\\prog", std::ios::binary | std::ios::ate);
    std::size_t size = file.tellg();
    file.seekg(0, std::ios::beg);
    std::vector<char> buffer(size);
    if (file.read(buffer.data(), size))
    {
        for (std::size_t i = 0; i < size; i++)
        {
            prog[i] = buffer[i];
        }
        foxlib::StartupROM rom(prog);
        vm.load_startup_rom(rom);
        vm.run();

        std::cout << "\nEND!\n";
    }

    
}
