#pragma once

#include <cstdint>
#include <queue>
#include <optional>
#include <stdexcept>

namespace foxlib {
    class FXBus;
    
    class BusConnectionI {
    public:
        BusConnectionI(FXBus* bus = nullptr);

        /* Master mode methods */
        void send_data(std::uint8_t address, std::uint8_t data);

        void recieve_data(std::uint8_t address);

        bool is_send_pending();

        bool is_send_complete();

        bool is_recieve_pending();

        bool is_recieve_complete();
        
        std::uint8_t get_recieved_data();

        /* Slave mode methods */
        virtual bool on_send_package(std::uint8_t address, std::uint8_t data) = 0;

        virtual std::optional<std::uint8_t> on_recieve_package(std::uint8_t address) = 0;

        friend FXBus;
    private:
        FXBus* bus;

        bool send_pending, send_complete;
        bool recieve_pending, recieve_complete;
        std::uint8_t recieved_data;
    };
    /*
        FXBus object handles bus which transfer data from different devices.
    */
    class FXBus {
    public:
        FXBus() = default;

        void send_data(std::uint8_t address, std::uint8_t data, BusConnectionI* connection) {
            bus_queue.push({ address, data, BusPacket::SEND, connection });
        }

        void recieve_data(std::uint8_t address, BusConnectionI* connection) {
            bus_queue.push({ address, 0u, BusPacket::RECIEVE, connection });
        }
    
        void resolve_queue(std::size_t resolve_count) {
            for (int i = 0; i < resolve_count && bus_queue.size(); ++i) {
                const BusPacket& packet = bus_queue.front();
                if (packet.type == BusPacket::SEND) {
                    bool ack = false;
                    for (BusConnectionI* conn : connections) {
                        if (conn != packet.connection) {
                            ack |= conn->on_send_package(packet.address, packet.data);
                        }
                    }
                    packet.connection->send_pending = false;
                    packet.connection->send_complete = ack;
                } else {
                    std::optional<std::uint8_t> data;
                    for (BusConnectionI* conn : connections) {
                        if (conn != packet.connection) {
                            auto new_data = conn->on_recieve_package(packet.address);
                            if (data && new_data) {
                                throw std::runtime_error("Multiple devices sent data at the same time");
                            }
                            data = new_data;
                        }
                    }
                    packet.connection->recieve_pending = false;
                    packet.connection->recieve_complete = data.has_value();
                    packet.connection->recieved_data = data.value_or(0u);
                }
                bus_queue.pop();
            }
        }

        void connect(BusConnectionI* connection) {
            connections.push_back(connection);
            connection->bus = this;
        }
        
    private:
        struct BusPacket {
            std::uint8_t address;
            std::uint8_t data;
            enum PacketType {
                SEND,
                RECIEVE
            } type;
            BusConnectionI* connection;
        };

        std::queue<BusPacket> bus_queue;
        std::vector<BusConnectionI*> connections;
    };

    class MasterBusConnection : public BusConnectionI {
    public:
        bool on_send_package(std::uint8_t address, std::uint8_t data) override {
            return false; // No acking
        }

        std::optional<std::uint8_t> on_recieve_package(std::uint8_t address) override {
            return std::nullopt; // No data
        }

    };


    /* IMPLEMENTATION */
    inline BusConnectionI::BusConnectionI(FXBus* bus) : bus(bus) {
        send_pending = false;
        send_complete = false;
        recieve_pending = false;
        recieve_complete = false;
    }

    inline void BusConnectionI::send_data(std::uint8_t address, std::uint8_t data) {
        bus->send_data(address, data, this);
        send_pending = true;
        send_complete = false;
    }

    inline void BusConnectionI::recieve_data(std::uint8_t address) {
        bus->recieve_data(address, this);
    }

    inline bool BusConnectionI::is_send_pending() {
        return send_pending;
    }

    inline bool BusConnectionI::is_send_complete() {
        return send_complete;
    }

    inline bool BusConnectionI::is_recieve_pending() {
        return recieve_pending;
    }

    inline bool BusConnectionI::is_recieve_complete() {
        return recieve_complete;
    }

    inline std::uint8_t BusConnectionI::get_recieved_data() {
        return recieved_data;
    }

}
